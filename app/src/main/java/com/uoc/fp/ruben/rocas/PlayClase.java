package com.uoc.fp.ruben.rocas;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import static java.lang.Math.abs;

public class PlayClase extends Activity implements OnClickListener, AdapterView.OnItemClickListener{

    private Button b1;
    private Button b2;
    private Button b3;
    private Button split_image;
    private LinearLayout split_numbers;
    private ImageView image;
    private GridView mGridView;
    private ArrayList<ImageData> chunkedImages;
    private ArrayList<ImageData> originalImages;
    private ImageAdapter imageAdapter;
    private  int selectedItem1 = -1;
    private  int selectedItem2 = -1;
    private ImageData item1;
    private ImageData item2;
    private Chronometer cmTimer;
    Boolean resume = false;
    long elapsedTime;
    String TAG = "TAG";
    private Button record;
    final Context context = this;
    private TextView txtResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play);

        b1 = (Button) findViewById(R.id.three);
        b2 = (Button) findViewById(R.id.four);
        b3 = (Button) findViewById(R.id.five);
        split_image = (Button)findViewById(R.id.split_image);
        split_numbers = (LinearLayout)findViewById(R.id.split_numbers);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);

        mGridView = (GridView) findViewById(R.id.gridview);

        mGridView.setOnItemClickListener(this);

        cmTimer = (Chronometer) findViewById(R.id.cmTimer);
        cmTimer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            public void onChronometerTick(Chronometer arg0) {
                if (!resume) {
                    long minutes = ((SystemClock.elapsedRealtime() - cmTimer.getBase())/1000) / 60;
                    long seconds = ((SystemClock.elapsedRealtime() - cmTimer.getBase())/1000) % 60;
                    elapsedTime = SystemClock.elapsedRealtime();
                    Log.d(TAG, "onChronometerTick: " + minutes + " : " + seconds);
                } else {
                    long minutes = ((elapsedTime - cmTimer.getBase())/1000) / 60;
                    long seconds = ((elapsedTime - cmTimer.getBase())/1000) % 60;
                    elapsedTime = elapsedTime + 1000;
                    Log.d(TAG, "onChronometerTick: " + minutes + " : " + seconds);
                }
            }
        });
        record = (Button) findViewById(R.id.buttonPrompt);


    }
    @Override
    public void onClick(View view){

        //chunkNumbers is to tell how many chunks the image should split
        int chunkNumbers = 0;

        /*
         * switch-case is used to find the button clicked
         * and assigning the actual value to chunkNumbers variable
         */

        switch (view.getId()) {
            case R.id.three:
                chunkNumbers = 9 ;
                break;
            case R.id.four:
                chunkNumbers = 16 ;
                break;
            case R.id.five:
                chunkNumbers = 25 ;
        }
        //Getting the source image to split
        image = (ImageView) findViewById(R.id.source_image);

        //invoking this method makes the actual splitting of the source image to given number of chunks
        splitImage(image, chunkNumbers);
        if (!resume) {
            cmTimer.setBase(SystemClock.elapsedRealtime());
            cmTimer.start();
        } else {
            cmTimer.start();
        }

    }
    private int[] getBitmapPositionInsideImageView(ImageView imageView) {
        int[] ret = new int[4];

        if (imageView == null || imageView.getDrawable() == null)
            return ret;

        // Get image dimensions
        // Get image matrix values and place them in an array
        float[] f = new float[9];
        imageView.getImageMatrix().getValues(f);

        // Extract the scale values using the constants (if aspect ratio maintained, scaleX == scaleY)
        final float scaleX = f[Matrix.MSCALE_X];
        final float scaleY = f[Matrix.MSCALE_Y];

        // Get the drawable (could also get the bitmap behind the drawable and getWidth/getHeight)
        final Drawable d = imageView.getDrawable();
        final int origW = d.getIntrinsicWidth();
        final int origH = d.getIntrinsicHeight();

        // Calculate the actual dimensions
        final int actW = Math.round(origW * scaleX);
        final int actH = Math.round(origH * scaleY);

        ret[2] = actW;
        ret[3] = actH;

        // Get image position
        // We assume that the image is centered into ImageView
        int imgViewW = imageView.getWidth();
        int imgViewH = imageView.getHeight();

        int top = (int) (imgViewH - actH)/2;
        int left = (int) (imgViewW - actW)/2;

        ret[0] = left;
        ret[1] = top;

        return ret;
    }
    private void splitImage(ImageView image, int chunkNumbers) {

        int rows,cols;
        rows = cols = (int) Math.sqrt(chunkNumbers);
        chunkedImages = new ArrayList<ImageData>(chunkNumbers);
        originalImages = new ArrayList<ImageData>(chunkNumbers);
        BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        int[] dimensions = getBitmapPositionInsideImageView(image);
        int scaledBitmapLeft = dimensions[0];
        int scaledBitmapTop = dimensions[1];
        int scaledBitmapWidth = dimensions[2];
        int scaledBitmapHeight = dimensions[3];

        int croppedImageWidth = scaledBitmapWidth - 2 * abs(scaledBitmapLeft);
        int croppedImageHeight = scaledBitmapHeight - 2 * abs(scaledBitmapTop);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, scaledBitmapWidth, scaledBitmapHeight, true);
        Bitmap croppedBitmap = Bitmap.createBitmap(scaledBitmap, abs(scaledBitmapLeft), abs(scaledBitmapTop), croppedImageWidth, croppedImageHeight);

        // Calculate the with and height of the pieces
        int pieceWidth = croppedImageWidth/cols;
        int pieceHeight = croppedImageHeight/rows;

        // Create each bitmap piece and add it to the resulting array
        int yCoord = 0;
        int sequenceNumber = 0;
        for (int row = 0; row < rows; row++) {
            int xCoord = 0;
            for (int col = 0; col < cols; col++) {

                chunkedImages.add(new ImageData(Bitmap.createBitmap(croppedBitmap, xCoord, yCoord, pieceWidth, pieceHeight),sequenceNumber));
                originalImages.add(new ImageData(Bitmap.createBitmap(croppedBitmap, xCoord, yCoord, pieceWidth, pieceHeight),sequenceNumber));
                xCoord += pieceWidth;
                sequenceNumber++;
            }
            yCoord += pieceHeight;
        }

        Collections.shuffle(chunkedImages);

        mGridView.setVisibility(View.VISIBLE);

        b1.setVisibility(View.GONE);
        b2.setVisibility(View.GONE);
        b3.setVisibility(View.GONE);

        image.setVisibility(View.GONE);

        split_image.setVisibility(View.GONE);
        split_numbers.setVisibility(View.GONE);

        imageAdapter = new ImageAdapter(this, chunkedImages);
        mGridView.setAdapter(imageAdapter);
        mGridView.setNumColumns((int) Math.sqrt(chunkedImages.size()));
        cmTimer.setVisibility(View.VISIBLE);


    }
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        //assuming your grid is a list of string elements
        //Bitmap item = (Bitmap)parent.getItemAtPosition(position);
        ImageData[]a = new ImageData[0];
        ImageData[]b;
       if (selectedItem1 == -1){
            selectedItem1 = position;
            item1 = (ImageData) parent.getItemAtPosition(selectedItem1);
        } else {
            selectedItem2 = position;
            item2 = (ImageData) parent.getItemAtPosition(selectedItem2);
            chunkedImages.set(selectedItem1,item2);
            chunkedImages.set(selectedItem2,item1);
            selectedItem1 = -1;
            selectedItem2 = -1;
        }
        imageAdapter.notifyDataSetChanged();
        win();

        //Toast.makeText(this,"position: "+position+"sequenceNumber: "+item.getSequenceNumber(), Toast.LENGTH_LONG).show();

    }
    private void win(){

        int lastSequenceNumber = 0;
        boolean isgood = true;
        for (ImageData imageData : chunkedImages) {
            if (imageData.getSequenceNumber() != lastSequenceNumber++){
                isgood=false;
                break;
            }
        }
        if(isgood){
            Toast.makeText(this,"You Win", Toast.LENGTH_LONG).show();
            cmTimer.stop();
           // cmTimer.setText("");
            resume = true;
            // Gets the data repository in write mode
            final SQLiteDatabase db =  MainActivity.gestorDataBase.getWritableDatabase();

            mGridView.setEnabled(false);
            record.setVisibility(View.VISIBLE);
            record = findViewById(R.id.buttonPrompt);


            record.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button

                    // get dialog_sigin.xml view
                    LayoutInflater li = LayoutInflater.from(context);
                    View promptsView = li.inflate(R.layout.dialog_signin, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                    // set prompts.xml to alertdialog builder
                    alertDialogBuilder.setView(promptsView);

                    final EditText userInput = (EditText) promptsView
                            .findViewById(R.id.editTextDialogUserInput);

                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,int id) {
                                            // get user input and set it to result
                                            // edit text
                                            // result.setText(userInput.getText());
                                            // Create a new map of values, where column names are the keys
                                            ContentValues values = new ContentValues();
                                            values.put(Resultados.COLUMN_NAME_NOMBRE, userInput.getText().toString());
                                            values.put(Resultados.COLUMN_NAME_TIEMPO, cmTimer.getText().toString());

                                            // Insert the new row, returning the primary key value of the new row
                                            long newRowId = db.insert(Resultados.TABLE_NAME, null, values);

                                            // Define a projection that specifies which columns from the database
                                            // you will actually use after this query.
                                            String[] projection = {
                                                    BaseColumns._ID,
                                                    Resultados.COLUMN_NAME_NOMBRE,
                                                    Resultados.COLUMN_NAME_TIEMPO
                                            };

                                            // String selection = Resultados.COLUMN_NAME_NOMBRE + " = ?";
                                            // String[] selectionArgs = { userInput.getText().toString() };

                                            // How you want the results sorted in the resulting Cursor
                                            String sortOrder = 
                                                    Resultados.COLUMN_NAME_TIEMPO;

                                            Cursor cursor = db.query(
                                                    Resultados.TABLE_NAME,   // The table to query
                                                    null,             // projection, ->The array of columns to return (pass null to get all)
                                                    null,              // The columns for the WHERE clause
                                                    null,          // The values for the WHERE clause
                                                    null,                   // don't group the rows
                                                    null,                   // don't filter by row groups
                                                    sortOrder               // The sort order
                                            );
                                            setContentView(R.layout.score);
                                            txtResultado = (TextView)findViewById(R.id.txtResultado);
                                            txtResultado.setText("");
                                            //Nos aseguramos de que existe al menos un registro
                                            if (cursor.moveToFirst()) {
                                                //Recorremos el cursor hasta que no haya más registros
                                                do {
                                                    String cod = cursor.getString(1);
                                                    String nom = cursor.getString(2);

                                                    txtResultado.append(" " + cod + " - " + nom + "\n");
                                                } while(cursor.moveToNext());
                                            }
                                        }
                                    })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,int id) {
                                            dialog.cancel();
                                        }
                                    });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }
            });
        }
    }
}