package com.uoc.fp.ruben.rocas;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;

import static com.uoc.fp.ruben.rocas.AudioService.*;

public class MainActivity extends AppCompatActivity {
    public static GestorDataBase gestorDataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gestorDataBase = new GestorDataBase(this);
    }

    public void ejecutar_play (View view){
        Intent i=new Intent(this,PlayClase.class);
        startActivity(i);
    }
    public void ejecutar_config (View view){
        Intent i=new Intent(this,Settings.class);
        startActivity(i);
    }
    public void ejecutar_info (View view){
        Intent i=new Intent(this,InfoClase.class);
        startActivity(i);
    }

    public void salirApp (View view){
        finish();
    }

    @Override public boolean onCreateOptionsMenu (Menu mimenu){

        getMenuInflater().inflate(R.menu.menu_en_activity, mimenu);
        return true;
    }
    @Override public boolean onOptionsItemSelected(MenuItem opcion_menu){
        int id=opcion_menu.getItemId();

        if(id==R.id.inicio){
            ejecutar_play(null);
            return true;
        }
        if(id==R.id.configuracion){
            ejecutar_config(null);
            return true;
        }
        if(id==R.id.info){
            ejecutar_info(null);
            return true;
        }
        return super.onOptionsItemSelected(opcion_menu);

    }
    @Override
    public void onPause() {
        super.onPause();
        //pausar();
        Intent i = new Intent(this, AudioService.class);
        i.putExtra("action", PAUSE);
        startService(i);
    }
    @Override
    public void onResume() {
        super.onResume();
        Intent i = new Intent(this, AudioService.class);
        i.putExtra("action", START);
        startService(i);
    }
}