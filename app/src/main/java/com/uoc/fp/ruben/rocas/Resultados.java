package com.uoc.fp.ruben.rocas;

import android.provider.BaseColumns;

public class Resultados implements BaseColumns {
    public static final String TABLE_NAME = "resultados";
    public static final String COLUMN_NAME_NOMBRE = "nombre";
    public static final String COLUMN_NAME_TIEMPO = "tiempo";
}